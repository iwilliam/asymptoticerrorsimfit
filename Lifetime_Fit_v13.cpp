#include "RootHeaders.h"
#include "RooFitHeaders.h"

#include "iostream"
#include "fstream"

using namespace RooFit;
using namespace RooStats;
using namespace TMath;
using namespace std;

#include "RooNewBinAccExponential.h"
#include "RooBinAccExponential.h"
#include "RooDoubleCBShape.h"
#include "Lifetime_Fit_v13.h"

// Main piece of code
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void Pull_Study() {

  cout << "Hello there" << endl;

  /// The default set up
  TString Conditions = "Run_II";
  Double_t Luminosity = 0;
  Double_t FitMin = 5320;
  Double_t FitMax = 6000;
  bool Fix_CBG_Slope = false;
  bool UseRunIIPDF = false;
  string Acceptance = "Default";
  Double_t Bs2mumu_lifetime_val = BsH_Lifetime;
  bool LHC_BFs = false;

  cout << "The default fit settings are: " << endl;
  cout << "   Conditions = Run_II (2011, 2012, 2015, 2016, 2017 and 2018 data)" << endl;
  cout << "   FitMin = 5320" << endl;
  cout << "   FitMax = 6000" << endl;
  cout << "   Fix_CBG_Slope = false" << endl;
  cout << "   Use Run II mass PDF for fit  = false" << endl;
  cout << "   Acceptance = Default" << endl;
  cout << "   Bs2mumu_lifetime_val = BsH_Lifetime" << endl;
  cout << "   Using SM branching fractions for Bsmm" << endl;
       
  int nToys = 1;

  cout << "You have requested " << nToys << " studies" << endl;
  //RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL); 
  //RooMsgService::instance().setSilentMode(true);

  int seed = 1;
  RooRandom::randomGenerator()->SetSeed(seed);
  TRandom3 randNo(seed);
      
  cout << "Performing simultaneous fit" << endl;

  //Set the number of candidates to generate for BDT bin 1
      
  // Set the level of signal and background according to the BDT cut 
  Double_t BDT_RatiosBin1[2] = { 1.0, 1.0 }; // Bs2mumu, CBG

  Double_t BDT_PhysRatioBin1 = Get_BDT_PhysRatio(0.35, 0.55);
  Double_t BDT_CBGRatioBin1  = Get_BDT_CBGRatio(0.35, 0.55);
    
  BDT_RatiosBin1[0] = BDT_PhysRatioBin1; 
  BDT_RatiosBin1[1] = BDT_CBGRatioBin1; 

  Double_t nCandsBin1[2] = { 0.0, 0.0 };
   
  if( Conditions == "Run_I") {
    nCandsBin1[0] = round(Bs2mumu_Yield_Run_I_SM_LT * BDT_RatiosBin1[0]);
    nCandsBin1[1] = round(CBG_Yield_Run_I           * BDT_RatiosBin1[1]);

    if(LHC_BFs) {
      nCandsBin1[0] = round(Bs2mumu_Yield_Run_I_LHC_LT * BDT_RatiosBin1[0]);
    }
  }
  else if( Conditions == "Run_I_plus_2015") {
    nCandsBin1[0] = round((Bs2mumu_Yield_Run_I_SM_LT + Bs2mumu_Yield_2015_SM_LT) * BDT_RatiosBin1[0]);
    nCandsBin1[1] = round((CBG_Yield_Run_I + CBG_Yield_2015                    ) * BDT_RatiosBin1[1]);

    if(LHC_BFs) {
      nCandsBin1[0] = round((Bs2mumu_Yield_Run_I_LHC_LT + Bs2mumu_Yield_2015_LHC_LT) * BDT_RatiosBin1[0]);
    }
  }
  else if( Conditions == "CKM16") {
    nCandsBin1[0] = round(Bs2mumu_Yield_CKM_SM_LT * BDT_RatiosBin1[0]);
    nCandsBin1[1] = round(CBG_Yield_CKM           * BDT_RatiosBin1[1]);

    if(LHC_BFs) {
      nCandsBin1[0] = round(Bs2mumu_Yield_CKM_LHC_LT * BDT_RatiosBin1[0]);
    }
  } 
  else if( Conditions == "Run_II") {
    nCandsBin1[0] = round(((Bs2mumu_Yield_Run_I_SM_LT) + (Bs2mumu_Yield_2016_SM_LT * nSPD_eff_16) * (Luminosity_Run_II/Luminosity_2016)) * BDT_RatiosBin1[0]);
    nCandsBin1[1] = round((CBG_Yield_Run_I + (CBG_Yield_2016 * nSPD_eff_16) * (Luminosity_Run_II/Luminosity_2016)                    ) * BDT_RatiosBin1[1]);
 
    if(LHC_BFs) {
      nCandsBin1[0] = round(((Bs2mumu_Yield_Run_I_LHC_LT) + (Bs2mumu_Yield_2016_LHC_LT * nSPD_eff_16) * (Luminosity_Run_II/Luminosity_2016)) * BDT_RatiosBin1[0]);
    }
  }
  else if( Conditions == "Run_III") {
    nCandsBin1[0] = round((Bs2mumu_Yield_Run_I_SM_LT + Bs2mumu_Yield_2016_SM_LT * (Luminosity_Run_III/Luminosity_2016)) * BDT_RatiosBin1[0]);
    nCandsBin1[1] = round((CBG_Yield_Run_I + CBG_Yield_2016 * (Luminosity_Run_III/Luminosity_2016)                    ) * BDT_RatiosBin1[1]);

    if(LHC_BFs) {
      nCandsBin1[0] = round((Bs2mumu_Yield_Run_I_LHC_LT + Bs2mumu_Yield_2016_LHC_LT * (Luminosity_Run_III/Luminosity_2016)) * BDT_RatiosBin1[0]);
    }
  }
  else {
    nCandsBin1[0] = round((Bs2mumu_Yield_Run_I_SM_LT + Bs2mumu_Yield_2016_SM_LT * (Luminosity/Luminosity_2016)) * BDT_RatiosBin1[0]);
    nCandsBin1[1] = round((CBG_Yield_Run_I + CBG_Yield_2016 * (Luminosity/Luminosity_2016)                    ) * BDT_RatiosBin1[1]);

    if(LHC_BFs) {
      nCandsBin1[0] = round((Bs2mumu_Yield_Run_I_LHC_LT + Bs2mumu_Yield_2016_LHC_LT * (Luminosity/Luminosity_2016)) * BDT_RatiosBin1[0]);
    }
  }

  //Set the number of candidates to generate for BDT bin 2
      
  // Set the level of signal and background according to the BDT cut 
  
  Double_t BDT_RatiosBin2[2] = { 1.0, 1.0 }; // Bs2mumu, CBG
  Double_t BDT_PhysRatioBin2 = Get_BDT_PhysRatio(0.55, 1.00);
  Double_t BDT_CBGRatioBin2  = Get_BDT_CBGRatio(0.55, 1.00);
      
  BDT_RatiosBin2[0] = BDT_PhysRatioBin2; 
  BDT_RatiosBin2[1] = BDT_CBGRatioBin2; 

  Double_t nCandsBin2[2] = { 0.0, 0.0 };
   
  if( Conditions == "Run_I") {
    nCandsBin2[0] = round(Bs2mumu_Yield_Run_I_SM_LT * BDT_RatiosBin2[0]);
    nCandsBin2[1] = round(CBG_Yield_Run_I           * BDT_RatiosBin2[1]);

    if(LHC_BFs) {
      nCandsBin2[0] = round(Bs2mumu_Yield_Run_I_LHC_LT * BDT_RatiosBin2[0]);
    }
  }
  else if( Conditions == "Run_I_plus_2015") {
    nCandsBin2[0] = round((Bs2mumu_Yield_Run_I_SM_LT + Bs2mumu_Yield_2015_SM_LT) * BDT_RatiosBin2[0]);
    nCandsBin2[1] = round((CBG_Yield_Run_I + CBG_Yield_2015                    ) * BDT_RatiosBin2[1]);

    if(LHC_BFs) {
      nCandsBin2[0] = round((Bs2mumu_Yield_Run_I_LHC_LT + Bs2mumu_Yield_2015_LHC_LT) * BDT_RatiosBin2[0]);
    }
  }
  else if( Conditions == "CKM16") {
    nCandsBin2[0] = round(Bs2mumu_Yield_CKM_SM_LT * BDT_RatiosBin2[0]);
    nCandsBin2[1] = round(CBG_Yield_CKM           * BDT_RatiosBin2[1]);

    if(LHC_BFs) {
      nCandsBin2[0] = round(Bs2mumu_Yield_CKM_LHC_LT * BDT_RatiosBin2[0]);
    }
  }
    
  else if( Conditions == "Run_II") {
    nCandsBin2[0] = round(((Bs2mumu_Yield_Run_I_SM_LT) + (Bs2mumu_Yield_2016_SM_LT * nSPD_eff_16) * (Luminosity_Run_II/Luminosity_2016)) * BDT_RatiosBin2[0]);
    nCandsBin2[1] = round((CBG_Yield_Run_I + (CBG_Yield_2016 * nSPD_eff_16) * (Luminosity_Run_II/Luminosity_2016)                    ) * BDT_RatiosBin2[1]);

    if(LHC_BFs) {
      nCandsBin2[0] = round(((Bs2mumu_Yield_Run_I_LHC_LT) + (Bs2mumu_Yield_2016_LHC_LT * nSPD_eff_16) * (Luminosity_Run_II/Luminosity_2016)) * BDT_RatiosBin2[0]);
    }
  }
  else if( Conditions == "Run_III") {
    nCandsBin2[0] = round((Bs2mumu_Yield_Run_I_SM_LT + Bs2mumu_Yield_2016_SM_LT * (Luminosity_Run_III/Luminosity_2016)) * BDT_RatiosBin2[0]);
    nCandsBin2[1] = round((CBG_Yield_Run_I + CBG_Yield_2016 * (Luminosity_Run_III/Luminosity_2016)                    ) * BDT_RatiosBin2[1]);

    if(LHC_BFs) {
      nCandsBin2[0] = round((Bs2mumu_Yield_Run_I_LHC_LT + Bs2mumu_Yield_2016_LHC_LT * (Luminosity_Run_III/Luminosity_2016)) * BDT_RatiosBin2[0]);
    }
  }
  else {
    nCandsBin2[0] = round((Bs2mumu_Yield_Run_I_SM_LT + Bs2mumu_Yield_2016_SM_LT * (Luminosity/Luminosity_2016)) * BDT_RatiosBin2[0]);
    nCandsBin2[1] = round((CBG_Yield_Run_I + CBG_Yield_2016 * (Luminosity/Luminosity_2016)                    ) * BDT_RatiosBin2[1]);

    if(LHC_BFs) {
      nCandsBin2[0] = round((Bs2mumu_Yield_Run_I_LHC_LT + Bs2mumu_Yield_2016_LHC_LT * (Luminosity/Luminosity_2016)) * BDT_RatiosBin2[0]);
    }
  }

  // Make some canvases with nice plots
  gStyle->SetFrameBorderMode(0);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetPadBorderMode(0);
  gStyle->SetPadColor(0);
  gStyle->SetCanvasColor(0);
  gStyle->SetStatColor(0);
  gStyle->SetPalette(1);

  // set the paper & margin sizes
  gStyle->SetPaperSize(20,26);
  gStyle->SetPadTopMargin(0.05);
  gStyle->SetPadRightMargin(0.05); // increase for colz plots!!
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.14);

  Double_t TauMin = 0.00000001;
  Double_t TauMax = 15;

  if(LHC_BFs)
    Conditions += "_LHC_BFs";

  TString Label = "2BinSimultaneousFit_" + to_string((Int_t)FitMin) + "-" + to_string((Int_t)FitMax) + "_" + to_string(nToys) + "_studies_seed_" +  to_string(seed);

  TFile fPlots(Label + ".root", "RECREATE");

  fPlots.cd();

  //------------------------------------------------
  // Create mass and decay time variables
  //------------------------------------------------

  RooRealVar* B_s0_M = new RooRealVar("mass", "m_{#mu^{+}#mu^{-}}", FitMin, FitMax, "MeV"); 
  RooRealVar* B_s0_TAU = new RooRealVar("B_s0_TAU", "decay time", TauMin, TauMax, "ps");
  RooArgSet VarSet(*B_s0_M, *B_s0_TAU);

  //----------------------------------------------------------
  // Calculate the expected yields in the fit range for bin 1
  //----------------------------------------------------------

  Double_t Yield_in_Fit_Range_Bin1[2] = { 0.0, 0.0 };

  Yield_in_Fit_Range_Bin1[0] = Calculate_B2mumu_Yield      ( B_s0_M, nCandsBin1[0], Bs2mumu_mean_val, Bs2mumu_sigma_val, Bs2mumu_alpha_val_1, Bs2mumu_n_val_1, 
    Bs2mumu_alpha_val_2, Bs2mumu_n_val_2, "Bs2mumu");
  Yield_in_Fit_Range_Bin1[1] = Calculate_CBG_Yield         ( B_s0_M, nCandsBin1[1], CBG_k_val_simfit );

  //----------------------------------------------------------
  // Calculate the expected yields in the fit range for bin 2
  //----------------------------------------------------------

  Double_t Yield_in_Fit_Range_Bin2[2] = { 0.0, 0.0 };

  Yield_in_Fit_Range_Bin2[0] = Calculate_B2mumu_Yield      ( B_s0_M, nCandsBin2[0], Bs2mumu_mean_val, Bs2mumu_sigma_val, Bs2mumu_alpha_val_1, Bs2mumu_n_val_1,
    Bs2mumu_alpha_val_2, Bs2mumu_n_val_2, "Bs2mumu");
  Yield_in_Fit_Range_Bin2[1] = Calculate_CBG_Yield         ( B_s0_M, nCandsBin2[1], CBG_k_val_simfit );

  //--------------------------------------------------------
  // Create a tree to store the results of the study
  //--------------------------------------------------------

  TTree Results_Tree1("Results_Tree1", "Results_Tree1");

  double Results0[16], Results1[16], Results2[12], Results3[16];

  TTree Results_Tree2("Results_Tree2", "Results_Tree2");

  double Results4[16], Results5[16], Results6[12], Results7[16];

  TTree Lifetime_Tree("Lifetime_Tree", "Lifetime_Tree");

  double Results8[12], Results9[12];

  //--------------------------------------------------------
  // Perform nToys pseudoexperiments
  //--------------------------------------------------------

  Int_t m = 1;
    
  for(Int_t t = 0; t < nToys; t++) {
      
    // Check our progress in the loop
    if(t >= m*(nToys/1000)) {
      cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
      cout << endl;
      cout << "Performing study " << t << " of " << nToys << endl;
      cout << endl;
      cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
      m++;
    }

    // Actual yields bin 1 
        
    Int_t ActualYieldBin1[2] = {
      randNo.Poisson(Yield_in_Fit_Range_Bin1[0]), 
      randNo.Poisson(Yield_in_Fit_Range_Bin1[1]),
    };
    
    // Actual yields bin 2   
    
    Int_t ActualYieldBin2[2] = {
      randNo.Poisson(Yield_in_Fit_Range_Bin2[0]), 
      randNo.Poisson(Yield_in_Fit_Range_Bin2[1]),
    };

    // Produce tuples with yields and sWeighted data samples

    std::tuple<RooRealVar, RooRealVar, RooRealVar, RooDataSet> Tuple_Bin1 = Generate_sWeight ( B_s0_M, B_s0_TAU, VarSet, Yield_in_Fit_Range_Bin1, ActualYieldBin1,
          CBG_k_val_simfit, FitMin, FitMax, "Bin 1", Acceptance, UseRunIIPDF, Fix_CBG_Slope, Bs2mumu_lifetime_val, CBG_lifetimeS_val_Bin1, CBG_lifetimeL_val_Bin1, CBG_fracS_val_Bin1, t);
  
    std::tuple<RooRealVar, RooRealVar, RooRealVar, RooDataSet> Tuple_Bin2 = Generate_sWeight ( B_s0_M, B_s0_TAU, VarSet, Yield_in_Fit_Range_Bin2, ActualYieldBin2, 
          CBG_k_val_simfit, FitMin, FitMax, "Bin 2", Acceptance, UseRunIIPDF, Fix_CBG_Slope, Bs2mumu_lifetime_val, CBG_lifetimeS_val_Bin2, CBG_lifetimeL_val_Bin2, CBG_fracS_val_Bin2, t);

   // Reassign tuple elements to variables
    
    RooDataSet wDataBin1 = std::get<3>(Tuple_Bin1);
    RooDataSet wDataBin2 = std::get<3>(Tuple_Bin2);

    RooRealVar Bs2mumu_Yield_res_Bin1 = std::get<0>(Tuple_Bin1);
    RooRealVar Bs2mumu_Yield_res_Bin2 = std::get<0>(Tuple_Bin2);

    if(Bs2mumu_Yield_res_Bin1.getVal() < 1.0){
      Bs2mumu_Yield_res_Bin1.setVal(0.0);
    }

    RooRealVar CBG_Yield_res_Bin1 = std::get<1>(Tuple_Bin1);
    RooRealVar CBG_Yield_res_Bin2 = std::get<1>(Tuple_Bin2);

    RooRealVar CBG_k_res_Bin1 = std::get<2>(Tuple_Bin1);
    RooRealVar CBG_k_res_Bin2 = std::get<2>(Tuple_Bin2);

    
  //--------------------------------------------------------------------------------------
  // Perform simultaneous fit to sWeighted data samples
  //--------------------------------------------------------------------------------------

    //Gaussian constraints on acceptance parameters

    /*RooRealVar A_bin1_acc_val("A_bin1_acc_val","A_bin1_acc_val",  a_acc_val_bin1, (a_acc_val_bin1 - (5 * a_acc_val_bin1_err)), (a_acc_val_bin1 + (5 * a_acc_val_bin1_err)));
    RooRealVar B_bin1_acc_val("B_bin1_acc_val","B_bin1_acc_val",  b_acc_val_bin1, (b_acc_val_bin1 - (5 * b_acc_val_bin1_err)), (b_acc_val_bin1 + (5 * b_acc_val_bin1_err)));
    RooRealVar C_bin1_acc_val("C_bin1_acc_val","C_bin1_acc_val",  c_acc_val_bin1, (c_acc_val_bin1 - (5 * c_acc_val_bin1_err)), (c_acc_val_bin1 + (5 * c_acc_val_bin1_err)));
    RooRealVar D_bin1_acc_val("D_bin1_acc_val","D_bin1_acc_val",  d_acc_val_bin1, (d_acc_val_bin1 - (5 * d_acc_val_bin1_err)), (d_acc_val_bin1 + (5 * d_acc_val_bin1_err)));
    RooRealVar E_bin1_acc_val("E_bin1_acc_val","E_bin1_acc_val",  e_acc_val_bin1, (e_acc_val_bin1 - (5 * e_acc_val_bin1_err)), (e_acc_val_bin1 + (5 * e_acc_val_bin1_err)));

    RooRealVar A_bin2_acc_val("A_bin2_acc_val","A_bin2_acc_val",  a_acc_val_bin2, (a_acc_val_bin2 - (5 * a_acc_val_bin2_err)), (a_acc_val_bin2 + (5 * a_acc_val_bin2_err)));
    RooRealVar B_bin2_acc_val("B_bin2_acc_val","B_bin2_acc_val",  b_acc_val_bin2, (b_acc_val_bin2 - (5 * b_acc_val_bin2_err)), (b_acc_val_bin2 + ( 5 * b_acc_val_bin2_err)));
    RooRealVar T0_bin2_acc_val("T0_bin2_acc_val","T0_bin2_acc_val",  t0_acc_val_bin2, (t0_acc_val_bin2 - (5 * t0_acc_val_bin2_err)), (t0_acc_val_bin2 + ( 5 * t0_acc_val_bin2_err)));*/

    RooRealVar A_bin1_acc_val("A_bin1_acc_val","A_bin1_acc_val",  a_acc_val_bin1);
    RooRealVar B_bin1_acc_val("B_bin1_acc_val","B_bin1_acc_val",  b_acc_val_bin1);
    RooRealVar C_bin1_acc_val("C_bin1_acc_val","C_bin1_acc_val",  c_acc_val_bin1);
    RooRealVar D_bin1_acc_val("D_bin1_acc_val","D_bin1_acc_val",  d_acc_val_bin1);
    RooRealVar E_bin1_acc_val("E_bin1_acc_val","E_bin1_acc_val",  e_acc_val_bin1);

    RooRealVar A_bin2_acc_val("A_bin2_acc_val","A_bin2_acc_val",  a_acc_val_bin2);
    RooRealVar B_bin2_acc_val("B_bin2_acc_val","B_bin2_acc_val",  b_acc_val_bin2);
    RooRealVar T0_bin2_acc_val("T0_bin2_acc_val","T0_bin2_acc_val",  t0_acc_val_bin2);

    //Gaussian Constraints for fit

    RooGaussian abin1constraint("abin1constraint","abin1constraint",A_bin1_acc_val,RooConst(a_acc_val_bin1),RooConst(a_acc_val_bin1_err));
    RooGaussian bbin1constraint("bbin1constraint","bbin1constraint",B_bin1_acc_val,RooConst(b_acc_val_bin1),RooConst(b_acc_val_bin1_err));
    RooGaussian cbin1constraint("cbin1constraint","cbin1constraint",C_bin1_acc_val,RooConst(c_acc_val_bin1),RooConst(c_acc_val_bin1_err));
    RooGaussian dbin1constraint("dbin1constraint","dbin1constraint",D_bin1_acc_val,RooConst(d_acc_val_bin1),RooConst(d_acc_val_bin1_err));
    RooGaussian ebin1constraint("ebin1constraint","ebin1constraint",E_bin1_acc_val,RooConst(e_acc_val_bin1),RooConst(e_acc_val_bin1_err));
    RooGaussian t0bin2constraint("t0bin2constraint","t0bin2constraint",T0_bin2_acc_val,RooConst(t0_acc_val_bin2),RooConst(t0_acc_val_bin2_err)) ;
    RooGaussian abin2constraint("abin2constraint","abin2constraint",A_bin2_acc_val,RooConst(a_acc_val_bin2),RooConst(a_acc_val_bin2_err)) ;
    RooGaussian bbin2constraint("bbin2constraint","bbin2constraint",B_bin2_acc_val,RooConst(b_acc_val_bin2),RooConst(b_acc_val_bin2_err));

    RooArgSet constraints(abin1constraint, bbin1constraint, cbin1constraint, dbin1constraint, ebin1constraint, t0bin2constraint, abin2constraint, bbin2constraint);
    
    //------------------------------------------------
    // Simultaneous fit for the lifetime
    //------------------------------------------------

    // Create observables

    RooRealVar    Bs2mumu_lifetime   ("Bs2mumu_lifetime"   , "Bs2mumu_lifetime"   , Bs_Lifetime, -10.0, 10.0, "ps");
    RooFormulaVar Bs2mumu_invLifetime("Bs2mumu_invLifetime", "Bs2mumu_invLifetime", "-1/@0", RooArgList(Bs2mumu_lifetime));

    // Construct Bin 1 pdf

    RooExponential Bs2mumu_Time_Lifetime_Exp_Bin1("Bs2mumu_Time_Lifetime_Exp_Bin1","Bs2mumu_Time_Lifetime_Exp_Bin1", *B_s0_TAU,  Bs2mumu_invLifetime);
    RooNewBinAccExponential Bs2mumu_Time_Lifetime_AccExp_Bin1("Bs2mumu_Time_Lifetime_AccExp_Bin1", "Bs2mumu_Time_Lifetime_AccExp_Bin1", *B_s0_TAU, 
    A_bin1_acc_val, B_bin1_acc_val, C_bin1_acc_val, D_bin1_acc_val, E_bin1_acc_val, Bs2mumu_invLifetime); 

    RooAbsPdf* Bs2mumu_Time_Lifetime_PDF_Bin1 = &Bs2mumu_Time_Lifetime_AccExp_Bin1;
    if(Acceptance == "Flat")
        Bs2mumu_Time_Lifetime_PDF_Bin1 = &Bs2mumu_Time_Lifetime_Exp_Bin1;

   // Construct Bin 2 pdf

    RooExponential Bs2mumu_Time_Lifetime_Exp_Bin2("Bs2mumu_Time_Lifetime_Exp_Bin2","Bs2mumu_Time_Lifetime_Exp_Bin2", *B_s0_TAU,  Bs2mumu_invLifetime);
    RooBinAccExponential Bs2mumu_Time_Lifetime_AccExp_Bin2 ("Bs2mumu_Time_Lifetime_AccExp_Bin2", "Bs2mumu_Time_Lifetime_AccExp_Bin2", *B_s0_TAU, T0_bin2_acc_val, A_bin2_acc_val, B_bin2_acc_val,
     Bs2mumu_invLifetime);

    RooAbsPdf* Bs2mumu_Time_Lifetime_PDF_Bin2 = &Bs2mumu_Time_Lifetime_AccExp_Bin2;
    if(Acceptance == "Flat")
        Bs2mumu_Time_Lifetime_PDF_Bin2 = &Bs2mumu_Time_Lifetime_Exp_Bin2;

    // Define category to distinguish BDT bins samples 
    RooCategory BDTCat("BDTCat","BDTCat");
    BDTCat.defineType("bin1");
    BDTCat.defineType("bin2");

    // Construct a combined dataset in (B_s0_TAU, BDTCat)
    
    RooRealVar sW2("sW2", "sW2", -1000.0, 1000.0);

    RooDataSet combData("combData", "combined data", RooArgSet(*B_s0_TAU, sW2), Index(BDTCat), WeightVar(sW2.GetName()), Import("bin1", wDataBin1),
    Import("bin2", wDataBin2));

    // Construct a simultaneous pdf using BDTCat as an index
    
    RooSimultaneous simPDF1("simPDF1", "simultaneous pdf", BDTCat);
    simPDF1.addPdf(*Bs2mumu_Time_Lifetime_PDF_Bin1, "bin1");
    simPDF1.addPdf(*Bs2mumu_Time_Lifetime_PDF_Bin2, "bin2");

  //------------------------------------------------
  // Simultaneous fit for the inverse lifetime
  //------------------------------------------------

    // Create observables

    RooRealVar    Bs2mumu_gamma   ("Bs2mumu_gamma"   , "Bs2mumu_gamma"   , 1.0/Bs_Lifetime, -10.0, 10.0, "ps");
    RooFormulaVar Bs2mumu_negGamma("Bs2mumu_negGamma", "Bs2mumu_negGamma", "-1.0*@0", RooArgList(Bs2mumu_gamma));

    // Construct Bin 1 pdf
    RooExponential Bs2mumu_Time_Gamma_Exp_Bin1("Bs2mumu_Time_Gamma_Exp_Bin1", "Bs2mumu_Time_Gamma_Exp_Bin1", *B_s0_TAU, Bs2mumu_negGamma);
    RooNewBinAccExponential Bs2mumu_Time_Gamma_AccExp_Bin1("Bs2mumu_Time_Gamma_AccExp_Bin1", "Bs2mumu_Time_Gamma_AccExp_Bin1", *B_s0_TAU, 
    A_bin1_acc_val, B_bin1_acc_val, C_bin1_acc_val, D_bin1_acc_val, E_bin1_acc_val, Bs2mumu_negGamma);

    RooAbsPdf* Bs2mumu_Time_Gamma_PDF_Bin1 = &Bs2mumu_Time_Gamma_AccExp_Bin1;
    if(Acceptance == "Flat")
        Bs2mumu_Time_Gamma_PDF_Bin1 = &Bs2mumu_Time_Gamma_Exp_Bin1;

    // Construct Bin 2 pdf
    RooExponential Bs2mumu_Time_Gamma_Exp_Bin2("Bs2mumu_Time_Gamma_Exp_Bin2", "Bs2mumu_Time_Gamma_Exp_Bin2", *B_s0_TAU, Bs2mumu_negGamma);
    RooBinAccExponential Bs2mumu_Time_Gamma_AccExp_Bin2("Bs2mumu_Time_Gamma_Exp_Bin2", "Bs2mumu_Time_Gamma_Exp_Bin2",  *B_s0_TAU, T0_bin2_acc_val, A_bin2_acc_val, B_bin2_acc_val,
     Bs2mumu_negGamma);
    RooAbsPdf* Bs2mumu_Time_Gamma_PDF_Bin2 = &Bs2mumu_Time_Gamma_AccExp_Bin2;
    if(Acceptance == "Flat")
        Bs2mumu_Time_Gamma_PDF_Bin2 = &Bs2mumu_Time_Gamma_Exp_Bin2;


    // Construct a simultaneous pdf using BDTCat as an index
  
    RooSimultaneous simPDF2("simPDF2", "simultaneous pdf", BDTCat);
    simPDF2.addPdf(*Bs2mumu_Time_Gamma_PDF_Bin1, "bin1");
    simPDF2.addPdf(*Bs2mumu_Time_Gamma_PDF_Bin2, "bin2");
   
    B_s0_TAU->setRange("range_bin1", 0.26, 15);
    B_s0_TAU->setRange("range_bin2", t0_acc_val_bin2, 15);
    
    // Perform a simultaneous fit of all exponential functions to their respective data sets

    if(t > 0) {
    //RooFitResult * Fit1 = simPDF1.fitTo(combData, Range("range"), SplitRange(kTRUE), ExternalConstraints(constraints), Verbose(kFALSE), PrintLevel(-1), PrintEvalErrors(-1), Save()); //Only fit in this range
    //RooFitResult * Fit2 = simPDF2.fitTo(combData, Range("range"), SplitRange(kTRUE), ExternalConstraints(constraints), Verbose(kFALSE), PrintLevel(-1), PrintEvalErrors(-1), Save());

    RooFitResult * Fit1 = simPDF1.fitTo(combData, Range("range"), SplitRange(kTRUE), AsymptoticError(kTRUE), Verbose(kFALSE), PrintLevel(-1), PrintEvalErrors(-1), Save()); //Only fit in this range
    RooFitResult * Fit2 = simPDF2.fitTo(combData, Range("range"), SplitRange(kTRUE), AsymptoticError(kTRUE), Verbose(kFALSE), PrintLevel(-1), PrintEvalErrors(-1), Save());
    
    /*if(Fit1->status() == 4 || Fit2->status() == 4 || Fit1->covQual() != 3 || Fit2->covQual() != 3){
      cout << "FAILED TO CONVERGE - REPEATING FIT" << endl;
      t = t-1;
      continue;
    }*/
    }


    if(t == 0) {  
    //RooFitResult * Fit1 = simPDF1.fitTo(combData, Range("range"), ExternalConstraints(constraints), SplitRange(kTRUE), Save());
    //RooFitResult * Fit2 = simPDF2.fitTo(combData, Range("range"), ExternalConstraints(constraints), SplitRange(kTRUE), Save());
    RooFitResult * Fit1 = simPDF1.fitTo(combData, Range("range"), AsymptoticError(kTRUE), SplitRange(kTRUE), Save());
    RooFitResult * Fit2 = simPDF2.fitTo(combData, Range("range"), AsymptoticError(kTRUE), SplitRange(kTRUE), Save());
    /*  if(Fit1->status() == 4 || Fit2->status() == 4 || Fit1->covQual() != 3 || Fit2->covQual() != 3){
        cout << "FAILED TO CONVERGE - REPEATING FIT" << endl;
        t = t-1;
        continue;
    }*/


    //--------------------------------------------------------------
    // Extract the results of the fit and plot the fitted decay time
    //--------------------------------------------------------------     
    
    // Plot "bin1" slice of simultaneous PDF
    RooPlot* pTau_Fit1 = B_s0_TAU->frame(Bins(15), Title("Bin 1 Sample"));
    combData.plotOn(pTau_Fit1, DataError(RooAbsData::SumW2), Cut("BDTCat==BDTCat::bin1"));
    simPDF1.plotOn(pTau_Fit1, Slice(BDTCat, "bin1"), ProjWData(BDTCat, combData),LineColor(kBlue));
    simPDF1.plotOn(pTau_Fit1, Slice(BDTCat, "bin1"), Components("Bs2mumu_Time_Lifetime_PDF_Bin1"), ProjWData(BDTCat, combData));
    
    double chi_2_bin1 = pTau_Fit1->chiSquare();
    cout << "chi squared bin 1 lifetime =" << chi_2_bin1 << endl;

    simPDF1.paramOn(pTau_Fit1, Format("NEU", AutoPrecision(1)));
    simPDF2.plotOn(pTau_Fit1,  Slice(BDTCat, "bin1"), ProjWData(BDTCat, combData), LineColor(kRed),LineStyle(kDashed));
    simPDF2.plotOn(pTau_Fit1, Slice(BDTCat, "bin1"), Components("Bs2mumu_Time_Gamma_PDF_Bin1"), ProjWData(BDTCat, combData));
    simPDF2.paramOn(pTau_Fit1, Format("NEU", AutoPrecision(1)));
    
     // Plot "bin2" slice of simultaneous PDF
    RooPlot* pTau_Fit2 = B_s0_TAU->frame(Bins(15), Title("Bin 2 Sample"));
    combData.plotOn(pTau_Fit2, DataError(RooAbsData::SumW2), Cut("BDTCat==BDTCat::bin2"));
    simPDF1.plotOn(pTau_Fit2, Slice(BDTCat, "bin2"), ProjWData(BDTCat, combData),LineColor(kRed));
    simPDF1.plotOn(pTau_Fit2, Slice(BDTCat, "bin2"), Components("Bs2mumu_Time_Lifetime_PDF_Bin2"), ProjWData(BDTCat, combData));
    
    double chi_2_bin2 = pTau_Fit2->chiSquare();
    cout << "chi squared bin 2 lifetime =" << chi_2_bin2 << endl;

    simPDF1.paramOn(pTau_Fit2, Format("NEU", AutoPrecision(1)));
    simPDF2.plotOn(pTau_Fit2,  Slice(BDTCat, "bin2"), ProjWData(BDTCat, combData), LineColor(kRed),LineStyle(kDashed));
    simPDF2.plotOn(pTau_Fit2, Slice(BDTCat, "bin2"), Components("Bs2mumu_Time_Gamma_PDF_Bin2"), ProjWData(BDTCat, combData));
    simPDF2.paramOn(pTau_Fit2, Format("NEU", AutoPrecision(1)));

    TCanvas* c =new TCanvas("Fitted sWeighted decay time", "Fitted sWeighted decay time");
    c->Divide(2);
    c->cd(1) ; pTau_Fit1->Draw(); pTau_Fit1->Write();
    c->cd(2) ; pTau_Fit2->Draw(); pTau_Fit2->Write();

    cout << endl;
    cout << "------------------------------------------------" << endl;
    cout << endl;
    cout << "Results of the fits" << endl;
    cout << endl;
    cout << "Fitted Bs0->mumu lifetime = " <<  Bs2mumu_lifetime.getVal() << " +/- " << Bs2mumu_lifetime.getError() << " ps" << endl;
    cout << "Input value = " << Bs2mumu_lifetime_val << " ps" << endl;
    cout << "Fitted 1/Bs0->mumu lifetime = " <<  Bs2mumu_gamma.getVal() << " +/- " << Bs2mumu_gamma.getError() << " ps-1" << endl;
    cout << "Input value = " << 1.0/Bs2mumu_lifetime_val  << " ps-1" << endl;
    cout << endl;
    cout << "------------------------------------------------" << endl;
    
  }  

    // Fill the tree for each variable
    
    Fill_Yield_Tree( Results_Tree1, Bs2mumu_Yield_res_Bin1, Yield_in_Fit_Range_Bin1[0], ActualYieldBin1[0], Results0, t );
    Fill_Yield_Tree( Results_Tree1, CBG_Yield_res_Bin1    , Yield_in_Fit_Range_Bin1[1], ActualYieldBin1[1], Results1, t ); 
    Fill_Tree( Results_Tree1, CBG_k_res_Bin1, CBG_k_val_simfit, Results2, t );

    Results_Tree1.Fill();

    Fill_Yield_Tree( Results_Tree2, Bs2mumu_Yield_res_Bin2, Yield_in_Fit_Range_Bin2[0], ActualYieldBin2[0], Results4, t );
    Fill_Yield_Tree( Results_Tree2, CBG_Yield_res_Bin2, Yield_in_Fit_Range_Bin2[1], ActualYieldBin2[1], Results5, t ); 
    Fill_Tree( Results_Tree2, CBG_k_res_Bin2, CBG_k_val_simfit, Results6, t );
    
    Results_Tree2.Fill();

    Fill_Tree( Lifetime_Tree, Bs2mumu_lifetime, Bs2mumu_lifetime_val, Results8, t );
    Fill_Tree( Lifetime_Tree, Bs2mumu_gamma, 1.0/Bs2mumu_lifetime_val, Results9, t );

    Lifetime_Tree.Fill();

  } 

  // Loop and create multiple toys
  
  Results_Tree1.Write();
  Results_Tree2.Write();
  Lifetime_Tree.Write();

  fPlots.Close();

  return 0;

  }
  
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


