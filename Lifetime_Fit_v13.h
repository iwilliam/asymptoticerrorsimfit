#include "RooNewBinAccExponential.h"
#include "RooBinAccExponential.h"
#include "RooDoubleCBShape.h"
#include "RootHeaders.h"
#include "RooFitHeaders.h"

// Mass range
Double_t MassMin = 4900.0;
Double_t MassMax = 6000.0;

// Luminosity factors
Double_t Luminosity_2011 = 0.9627; // Taken from Table 1 in LHCb-ANA-2016-038 V6
Double_t Luminosity_2012 = 1.9732;
Double_t Luminosity_2015 = 0.2902;
Double_t Luminosity_2016 = 1.1; // Luminosity till the technical stop in 2016

Double_t Luminosity_Run_II = 6;   // 
Double_t Luminosity_Run_III = 50.0; 

// Run I yields in range 4900-6000 MeV for BDTflat > 0.6 and 3fb-1 selection
Double_t Bs2mumu_Run_I_Yield = 3.76 + 3.61 + 3.68 + 3.79;
Double_t CBG_Run_I_Yield = 64.4783;

// PID Efficiencies
//-----------------

Double_t B2mumu_BF_PID_Efficiency_Run_I = 0.8321;   // Taken from Table 6 in LHCb-ANA-2016-038 V6
Double_t B2mumu_BF_PID_Efficiency_2015 = 0.8347;    // The 'BF' numbers are the efficiencies for the Branching Fraction
Double_t B2mumu_BF_PID_Efficiency_2016 = 0.8788;    // analysis PID cuts of PIDmu > 0.4 for 2011, 2012 and 2015 and PIDmu > 0.8 for 2016
Double_t B2mumu_LT_PID_Efficiency_Run_I = 0.93922;  // The 'LT' numbers are taken from Marco Santimaria's slides from 26/10/16; 
Double_t B2mumu_LT_PID_Efficiency_2015 = 0.93207;   // the efficiencies of PID cuts of PIDmu > 0.2 for 2011, 2012 and 2015 and PIDmu > 0.4 for 2016
Double_t B2mumu_LT_PID_Efficiency_2016 = 0.96887;   //

// Particle properties
//--------------------

// Bs2mumu
Double_t Bs2mumu_Yield_Run_I_SM_BF_Full_BDT_Range = 10.2 + 22.9; // Taken from Table 53 in LHCb-ANA-2016-038 V6
Double_t Bs2mumu_Yield_2015_SM_BF_Full_BDT_Range = 5.6;          // The 'SM' numbers are for the SM prediction
Double_t Bs2mumu_Yield_2016_SM_BF_Full_BDT_Range = 22.8;         // The 'LHC' numbers are using the BFs from the CMS+LHCb+ATLAS analysis of 2020
Double_t Bs2mumu_Yield_Run_I_LHC_BF_Full_BDT_Range = 7.5 + 16.8; // All numbers are quoted in the full BDT 0-1 range with the BF PID selection:
Double_t Bs2mumu_Yield_2015_LHC_BF_Full_BDT_Range = 4.1;         // PIDmu > 0.4 for 2011, 2012 and 2015 and PIDmu > 0.8 for 2016
Double_t Bs2mumu_Yield_2016_LHC_BF_Full_BDT_Range = 16.8;        // 

Double_t Bs2mumu_Yield_Run_I_SM_LT = Bs2mumu_Yield_Run_I_SM_BF_Full_BDT_Range*(B2mumu_LT_PID_Efficiency_Run_I/B2mumu_BF_PID_Efficiency_Run_I)*(1.0-0.55);
Double_t Bs2mumu_Yield_2015_SM_LT = Bs2mumu_Yield_2015_SM_BF_Full_BDT_Range*(B2mumu_LT_PID_Efficiency_2015/B2mumu_BF_PID_Efficiency_2015)*(1.0-0.55);
Double_t Bs2mumu_Yield_2016_SM_LT = Bs2mumu_Yield_2016_SM_BF_Full_BDT_Range*(B2mumu_LT_PID_Efficiency_2016/B2mumu_BF_PID_Efficiency_2016)*(1.0-0.55);
Double_t Bs2mumu_Yield_CKM_SM_LT = Bs2mumu_Yield_Run_I_SM_LT + Bs2mumu_Yield_2015_SM_LT + Bs2mumu_Yield_2016_SM_LT;

Double_t Bs2mumu_Yield_Run_I_LHC_LT = Bs2mumu_Yield_Run_I_LHC_BF_Full_BDT_Range*(B2mumu_LT_PID_Efficiency_Run_I/B2mumu_BF_PID_Efficiency_Run_I)*(1.0-0.55);
Double_t Bs2mumu_Yield_2015_LHC_LT = Bs2mumu_Yield_2015_LHC_BF_Full_BDT_Range*(B2mumu_LT_PID_Efficiency_2015/B2mumu_BF_PID_Efficiency_2015)*(1.0-0.55);
Double_t Bs2mumu_Yield_2016_LHC_LT = Bs2mumu_Yield_2016_LHC_BF_Full_BDT_Range*(B2mumu_LT_PID_Efficiency_2016/B2mumu_BF_PID_Efficiency_2016)*(1.0-0.55);
Double_t Bs2mumu_Yield_CKM_LHC_LT = Bs2mumu_Yield_Run_I_LHC_LT + Bs2mumu_Yield_2015_LHC_LT + Bs2mumu_Yield_2016_LHC_LT;

Double_t Bs2mumu_mean_val = 5369.15; // These are the new numbers from the ANA note
Double_t Bs2mumu_sigma_val = 22.23;  // 
Double_t Bs2mumu_alpha_val_1 = 1.78;
Double_t Bs2mumu_n_val_1 = 1.51;
Double_t Bs2mumu_alpha_val_2 = 2.12;
Double_t Bs2mumu_n_val_2 = 5.97;

Double_t Bs2mumu_mean_val_prev = 5372.05; // These are the numbers from the previous ANA note
Double_t Bs2mumu_sigma_val_prev = 23.07;  // 
Double_t Bs2mumu_alpha_val_prev = 2.053;
Double_t Bs2mumu_n_val_prev = 1.156;

Double_t Bs_Lifetime = 1.512; //PDG
Double_t BsL_Lifetime = 1.405; // PDG
Double_t BsH_Lifetime = 1.661; // PDG

// Combinatorial background
Double_t CBG_Yield_Run_I = 30.95; // For the new selection including BDTflat1 > 0.55. Calculated from the original Run I analysis yield multiplied by the ratio of selection efficiencies taken from MC
Double_t CBG_Yield_2015 = CBG_Yield_Run_I*(Bs2mumu_Yield_2015_SM_LT/Bs2mumu_Yield_Run_I_LHC_LT); // These assume that the combinatorial backgrounds increases
Double_t CBG_Yield_2016 = CBG_Yield_Run_I*(Bs2mumu_Yield_2016_SM_LT/Bs2mumu_Yield_Run_I_LHC_LT); // in proportion to Bs2mumu decays
Double_t CBG_Yield_CKM = CBG_Yield_Run_I + CBG_Yield_2015 + CBG_Yield_2016;

// NB: All background yields are from Marco Santimaria's slides from 26/10/16 
// made assuming a BDT1flat > 0.55 and PIDmu > 0.2 (2011, 2012, 2015) and > 0.4 (2016)
// All parameters for the mass PDFs are also from the same slides, apart from B2hh which
// were determined using a fit to a cocktail of B2hh modes in MC.

// Combinatorial background

Double_t CBG_k_val_simfit = -0.0005; // rough average from ANA note page 32 footnote 5

// Acceptance parameters - Bin 1

//From fit to combined (2011-2018) B->hh sideband data sample

Double_t CBG_lifetimeS_val_Bin1 = 1.00703e+00; // from fit 1.05703; 1SD Version
Double_t CBG_lifetimeL_val_Bin1 = 4.34627e+00; //from fit; 5.68627; 1SD Version

Double_t CBG_fracS_val_Bin1 = 0.9273924; //from 0.847; 1SD Version

Double_t CBG_a_acc_val_Bin1 = 1.99999e+00; //from fit;
Double_t CBG_b_acc_val_Bin1 = 6.05889e-01; //from fit;
Double_t CBG_c_acc_val_Bin1 = 1.58396e+00; //from fit;
Double_t CBG_d_acc_val_Bin1 = 1.00029e-02; //from fit;
Double_t CBG_e_acc_val_Bin1 = 1.00001e-01; //from fit;

// Acceptance parameters - Bin 2

Double_t CBG_lifetimeS_val_Bin2 =  1.07341; //1.0/0.7723; 1.29341; 1SD Version
Double_t CBG_lifetimeL_val_Bin2 = 9.99538; //1.0/0.0564; 18.20538; 1SD Version

Double_t CBG_fracS_val_Bin2 =  0.9865164; //0.981; 1SD Version

Double_t CBG_t0_acc_val_Bin2 = 0.290;
Double_t CBG_a_acc_val_Bin2 = 1.56018e+00;
Double_t CBG_b_acc_val_Bin2 = 1.17667e+00;

// From fit to cocktail of combined (2011-2018) reweighted Bs->MuMu MC
Double_t a_acc_val_bin1 = 1.30087e+00;
Double_t b_acc_val_bin1 = 4.61762e-01;
Double_t c_acc_val_bin1 = 3.30001e+00;
Double_t d_acc_val_bin1 = 1.65136e-02;
Double_t e_acc_val_bin1 = 2.39368e-01;

Double_t a_acc_val_bin1_err = 4.83215e-03;
Double_t b_acc_val_bin1_err = 1.17246e-02;
Double_t c_acc_val_bin1_err = 1.66114e-02;
Double_t d_acc_val_bin1_err = 4.22160e-04;
Double_t e_acc_val_bin1_err = 1.29080e-03;

// Updated acceptance parameters from fit to cocktail of combined (2011-2018) reweighted Bs->MuMu MC
Double_t a_acc_val_bin2 = 2.13983e+00;
Double_t b_acc_val_bin2 = -1.26560e+00;
Double_t t0_acc_val_bin2 = 2.16891e-01;

Double_t a_acc_val_bin2_err = 1.86819e-02;
Double_t b_acc_val_bin2_err = 8.60216e-03;
Double_t t0_acc_val_bin2_err = 1.48316e-03;

Double_t nSPD_eff_16 = 0.96;

// Number of bins on mass plot
Int_t nBins = 25;

//-------------------------------------------------------------
//
// Function to calculate the BDT_PhysRatio
//
//------------------------------------------------------------

double Get_BDT_PhysRatio(double BDT_min, double BDT_max ) {

  double c = 1.0/(1.0-0.55);

  double BDT_PhysRatio = c*(BDT_max - BDT_min);

  return BDT_PhysRatio;

}

//-------------------------------------------------------------
//
// Function to calculate the BDT_CBGRatio
//
//------------------------------------------------------------

double Get_BDT_CBGRatio(double BDT_min, double BDT_max ) {

  double a = -0.0775855404;
  double c = -4.401027586;

  double BDT_CBGRatio = a*(pow(BDT_max, c) - pow(BDT_min,c));

  return BDT_CBGRatio;

}


//-------------------------------------------------------------
//
// Function to automatically format plots
//
//------------------------------------------------------------

void Format_Plot(RooPlot* plot) {
 
  plot->GetXaxis()->SetLabelOffset(0.015);
  plot->GetXaxis()->SetLabelSize(0.05);
  plot->GetXaxis()->SetTitleSize(0.05);
  plot->GetXaxis()->SetTitleOffset(1.3);
  plot->GetYaxis()->SetLabelSize(0.05);
  plot->GetYaxis()->SetTitleSize(0.05);
  plot->GetYaxis()->SetTitleOffset(1.3);
  plot->SetTitle("");

}

//--------------------------------------------------------------
//
// Function to calculate the fraction of a PDF in the fit range
//
//--------------------------------------------------------------

Double_t Calculate_Fit_Range_Yield(RooAbsPdf& PDF, RooRealVar& B_s0_M_Norm, Double_t Full_Range_Yield, TString Decay ) {

  Double_t PDF_Frac_in_Fit_Range = PDF.createIntegral(B_s0_M_Norm, NormSet(B_s0_M_Norm), Range("fit"))->getVal();
  Double_t Fit_Range_Yield = Full_Range_Yield*PDF_Frac_in_Fit_Range;
  
  cout << "INFO :: Calculate_Fit_Range_Yield :: Yield of " << Decay << " in full range " << B_s0_M_Norm.getMin() << " - " << B_s0_M_Norm.getMax() << " MeV = " << Full_Range_Yield << endl;
  cout << "INFO :: Calculate_Fit_Range_Yield :: Yield of " << Decay << " in fit  range " << B_s0_M_Norm.getMin("fit") << " - " << B_s0_M_Norm.getMax("fit") << " MeV =  " << Fit_Range_Yield << endl;
  
  return Fit_Range_Yield; 

}

//------------------------------------------------------------------
//
// Function to calculate how many B(s,d)2mumu candidates to generate
//
//------------------------------------------------------------------

Double_t Calculate_B2mumu_Yield( RooRealVar* B_s0_M, Double_t Full_Range_Yield, Double_t mean_val, Double_t sigma_val, Double_t alpha_val_1, 
  Double_t n_val_1, Double_t alpha_val_2, Double_t n_val_2, TString Decay) {

  // Setup a mass variable to be used in the full mass range for calculations
  RooRealVar B_s0_M_Norm("mass", "mass", MassMin, MassMax, "MeV/c^{2}");

  // Get the limits of the generation and fit range
  Double_t MassGenMin = B_s0_M->getMin();
  Double_t MassGenMax = B_s0_M->getMax();

  // Define a range
  B_s0_M_Norm.setRange("fit", MassGenMin, MassGenMax);

  // Setup the mass PDF used to calculate the number of candidates to generate
  RooRealVar mean ("mean" , "mean" , mean_val);
  RooRealVar sigma("sigma", "sigma", sigma_val);
  RooRealVar alpha_1("alpha_1", "alpha_1", alpha_val_1);
  RooRealVar alpha_2("alpha_2", "alpha_2", alpha_val_2);
  RooRealVar n_1    ("n_1"    , "n_1"    , n_val_1);
  RooRealVar n_2    ("n_2"    , "n_2"    , n_val_2);

  RooDoubleCBShape Mass_Norm_PDF("Mass_Norm_PDF", "Mass_Norm_PDF", B_s0_M_Norm, mean, sigma, alpha_1, n_1, alpha_2, n_2);

  // Calculate the fraction of the PDF in the generation/fit range
  Double_t Fit_Range_Yield = Calculate_Fit_Range_Yield(Mass_Norm_PDF, B_s0_M_Norm, Full_Range_Yield, Decay );

  return Fit_Range_Yield;

}

//-------------------------------------------------------------------------------
//
// Function to calculate how many combinatorial background candidates to generate
//
//-------------------------------------------------------------------------------

Double_t Calculate_CBG_Yield( RooRealVar* B_s0_M, Double_t Full_Range_Yield, Double_t k_val ) {

  // Setup a mass variable to be used in the full mass range for calculations
  RooRealVar B_s0_M_Norm("mass", "mass", MassMin, MassMax, "MeV/c^{2}");

  // Get the limits of the generation and fit range
  Double_t MassGenMin = B_s0_M->getMin();
  Double_t MassGenMax = B_s0_M->getMax();

  // Define a range
  B_s0_M_Norm.setRange("fit", MassGenMin, MassGenMax);

  // Setup the mass PDF used to calculate the number of candidates to generate
  RooRealVar k("k" , "k" , k_val);

  RooExponential Mass_Norm_PDF("Mass_Norm_PDF", "Mass_Norm_PDF", B_s0_M_Norm, k);

  // Calculate the fraction of the PDF in the generation/fit range
  Double_t Fit_Range_Yield = Calculate_Fit_Range_Yield(Mass_Norm_PDF, B_s0_M_Norm, Full_Range_Yield, "Combinatorial background" );

  return Fit_Range_Yield;

}

//------------------------------------------------------------------
//
// Function to generate B(s,d)2mumu candidates
//
//------------------------------------------------------------------

RooDataSet Generate_B2mumu( RooRealVar* B_s0_M, RooRealVar* B_s0_TAU, RooArgSet& VarSet, Int_t ActualYield,
          Double_t mean_val, Double_t sigma_val, Double_t alpha_val_1, Double_t n_val_1, Double_t alpha_val_2, 
          Double_t n_val_2, Double_t lifetime_val, TString BDTbin, TString Acceptance, TString Decay, Int_t t) {

  // Setup the mass PDF
  RooRealVar mean ("mean" , "mean" , mean_val);
  RooRealVar sigma("sigma", "sigma", sigma_val); 
  RooRealVar alpha_1("alpha_1", "alpha_1", alpha_val_1);
  RooRealVar alpha_2("alpha_2", "alpha_2", alpha_val_2);
  RooRealVar n_1    ("n_1"    , "n_1"    , n_val_1);
  RooRealVar n_2    ("n_2"    , "n_2"    , n_val_2);

  RooDoubleCBShape Mass_Gen_PDF("Mass_Gen_PDF", "Mass_Gen_PDF", *B_s0_M, mean, sigma, alpha_1, n_1, alpha_2, n_2);

  // Setup the decay time PDF
  RooRealVar    lifetime("lifetime", "lifetime", lifetime_val);
  RooFormulaVar gamma   ("gamma"   , "gamma"   , "-1/@0", RooArgList(lifetime));
  
  RooExponential    Time_Exp   ("Time_Exp"   , "Time_Exp"   , *B_s0_TAU, gamma);
  RooBinAccExponential Time_AccExp("Time_AccExp", "Time_AccExp", *B_s0_TAU, RooConst(t0_acc_val_bin2), RooConst(a_acc_val_bin2), RooConst(b_acc_val_bin2), gamma);
  RooNewBinAccExponential Time_AccExp_Bin1("Time_AccExp_Bin1", "Time_AccExp_Bin1", *B_s0_TAU, RooConst(a_acc_val_bin1), RooConst(b_acc_val_bin1), 
  RooConst(c_acc_val_bin1), RooConst(d_acc_val_bin1), RooConst(e_acc_val_bin1), gamma); 

  RooAbsPdf* Time_Gen_PDF = &Time_AccExp;
  
  if(BDTbin == "Bin 1")
    Time_Gen_PDF = &Time_AccExp_Bin1;
  if(Acceptance == "Flat")
    Time_Gen_PDF = &Time_Exp; 

  // Make the mass-time PDF
  RooProdPdf Gen_PDF("Gen_PDF", "Gen_PDF", RooArgList(Mass_Gen_PDF, *Time_Gen_PDF));
  RooDataSet Data = *(Gen_PDF.generate(VarSet, ActualYield, Verbose(false)));  // Note: extended generation is not used, instead the input yields are fluctuated using a poisson distribution

  if(t == 0) {
    cout << BDTbin << " " << "-" << " " << "INFO :: Generate_B2mumu :: Yield of " << Decay << " generated  =  " << ActualYield << endl;

    // Plot the data and generator PDFs
    TCanvas cMassGen(Decay + " generated mass -" + BDTbin, Decay + " generated mass -" + BDTbin, 1);
    RooPlot* pMassGen = B_s0_M->frame(Bins(nBins));
    Format_Plot(pMassGen); 
    Data.plotOn(pMassGen) ;
    Gen_PDF.plotOn(pMassGen);
    pMassGen->Draw();
    cMassGen.Write();
      
    // Plot the data and generator PDFs
    TCanvas cTimeGen(Decay + " generated decay time -" + BDTbin, Decay + " generated decay time -" + BDTbin, 1);
    RooPlot* pTimeGen = B_s0_TAU->frame(Bins(100));
    Format_Plot(pTimeGen);
    Data.plotOn(pTimeGen) ;
    Gen_PDF.plotOn(pTimeGen);
    pTimeGen->Draw();
    cTimeGen.Write();

  }

return Data;

}


//---------------------------------------------------------
//
// Function to generate combinatorial background candidates
//
//---------------------------------------------------------

RooDataSet Generate_CBG( RooRealVar* B_s0_M, RooRealVar* B_s0_TAU, RooArgSet& VarSet, Int_t ActualYield, 
       Double_t k_val, Double_t lifetimeS_val, Double_t fracS_val, Double_t lifetimeL_val, TString BDTbin, 
       TString Acceptance, Int_t t ) {

  // Setup the mass PDF 
  RooRealVar k("k" , "k" , k_val);

  RooExponential Mass_Gen_PDF("Mass_Gen_PDF", "Mass_Gen_PDF", *B_s0_M, k);

  // Setup the decay time PDF
  RooRealVar lifetime_S("lifetime_S", "lifetime_S", lifetimeS_val);
  RooFormulaVar gamma_S("gamma_S", "gamma_S", "-1/@0", RooArgList(lifetime_S));
  RooExponential Time_Exp_S("Time_Exp_S", "Time_Exp_S", *B_s0_TAU, gamma_S);
  RooBinAccExponential Time_AccExp_S("Time_AccExp_S", "Time_AccExp_S", *B_s0_TAU, RooConst(CBG_t0_acc_val_Bin2), RooConst(CBG_a_acc_val_Bin2), RooConst(CBG_b_acc_val_Bin2), gamma_S);
  RooNewBinAccExponential Time_AccExp_S_Bin1("Time_AccExp_S_Bin1", "Time_AccExp_S_Bin1", *B_s0_TAU, RooConst(CBG_a_acc_val_Bin1), RooConst(CBG_b_acc_val_Bin1), RooConst(CBG_c_acc_val_Bin1), 
    RooConst(CBG_d_acc_val_Bin1), RooConst(CBG_e_acc_val_Bin1), gamma_S);
  RooAbsPdf* Time_PDF_S = &Time_AccExp_S;
  if(BDTbin == "Bin 1")
    Time_PDF_S = &Time_AccExp_S_Bin1;
  if(Acceptance == "Flat")
    Time_PDF_S = &Time_Exp_S;

  RooRealVar lifetime_L("lifetime_L", "lifetime_L", lifetimeL_val);
  RooFormulaVar gamma_L("gamma_L", "gamma_L", "-1/@0", RooArgList(lifetime_L));
  RooExponential Time_Exp_L("Time_Exp_L", "Time_Exp_L", *B_s0_TAU, gamma_L);
  RooBinAccExponential Time_AccExp_L("Time_AccExp_L", "Time_AccExp_L", *B_s0_TAU, RooConst(CBG_t0_acc_val_Bin2), RooConst(CBG_a_acc_val_Bin2), RooConst(CBG_b_acc_val_Bin2), gamma_L);
  RooNewBinAccExponential Time_AccExp_L_Bin1("Time_AccExp_L_Bin1", "Time_AccExp_L_Bin1", *B_s0_TAU, RooConst(CBG_a_acc_val_Bin1), RooConst(CBG_b_acc_val_Bin1), RooConst(CBG_c_acc_val_Bin1),
  RooConst(CBG_d_acc_val_Bin1), RooConst(CBG_e_acc_val_Bin1), gamma_L);
  RooAbsPdf* Time_PDF_L = &Time_AccExp_L;
  if(BDTbin == "Bin 1")
    Time_PDF_L = &Time_AccExp_L_Bin1;
  if(Acceptance == "Flat")
    Time_PDF_L = &Time_Exp_L;

  RooRealVar frac_S("frac_S", "frac_S", fracS_val);

  RooAddPdf Time_Gen_PDF("Time_Gen_PDF", "Time_Gen_PDF", RooArgList(*Time_PDF_S, *Time_PDF_L), RooArgList(frac_S));

  // Mass vs decay time
  RooProdPdf Gen_PDF("Gen_PDF", "Gen_PDF", RooArgList(Mass_Gen_PDF, Time_Gen_PDF));

  // Generate the data
  RooDataSet Data = *(Gen_PDF.generate(VarSet, ActualYield, Verbose(false)));  // Note: extended generation is not used, instead the input yields are fluctuated using a poisson distribution
  
  //ActualYield = Data.sumEntries();
  if(t == 0) {
  
    cout << BDTbin << " " << "-" << " " << "INFO :: Generate_CBG :: Yield of combinatorial background generated  =  " << ActualYield << endl;
 
    // Plot the data and generator PDFs
    TCanvas cMassGen("CBG generated mass -" + BDTbin, "CBG generated mass -" + BDTbin, 1);
    RooPlot* pMassGen = B_s0_M->frame(Bins(nBins));
    Format_Plot(pMassGen); 
    Data.plotOn(pMassGen) ;
    Gen_PDF.plotOn(pMassGen);
    pMassGen->Draw();
    cMassGen.Write();
      
    // Plot the data and generator PDFs
    TCanvas cTimeGen("CBG generated decay time -" + BDTbin, "CBG generated decay time -" + BDTbin, 1);
    RooPlot* pTimeGen = B_s0_TAU->frame(Bins(100));
    Format_Plot(pTimeGen);
    Data.plotOn(pTimeGen) ;
    Gen_PDF.plotOn(pTimeGen);
    pTimeGen->Draw();
    cTimeGen.Write();

  }

  return Data;
}

////------------------------------------------------------------------
////
//// Function to generate yields and sWeighted data samples
////
////------------------------------------------------------------------

std::tuple<RooRealVar, RooRealVar, RooRealVar, RooDataSet> Generate_sWeight( RooRealVar* B_s0_M, RooRealVar* B_s0_TAU, RooArgSet& VarSet, Double_t Yield_in_Fit_Range[2], 
Int_t ActualYield[2], Double_t k_val, Double_t FitMin, Double_t FitMax, TString BDTbin, TString Acceptance, bool UseRunIIPDF, bool Fix_CBG_Slope, Double_t Bs2mumu_lifetime_val, 
Double_t CBG_lifetimeS_val, Double_t CBG_lifetimeL_val, Double_t CBG_fracS_val, Int_t t) {


  //------------------------------------------------
  // Generate the Data
  //------------------------------------------------
        
  RooDataSet Bs2mumu_Data = Generate_B2mumu(B_s0_M, B_s0_TAU, VarSet, ActualYield[0], Bs2mumu_mean_val, Bs2mumu_sigma_val, Bs2mumu_alpha_val_1, Bs2mumu_n_val_1, 
  Bs2mumu_alpha_val_2, Bs2mumu_n_val_2, Bs2mumu_lifetime_val, BDTbin, Acceptance, "Bs2mumu", t);
        
  RooDataSet CBG_Data = Generate_CBG(B_s0_M, B_s0_TAU, VarSet, ActualYield[1], k_val, CBG_lifetimeS_val, CBG_fracS_val, CBG_lifetimeL_val, BDTbin, Acceptance, t);

  // Create the total dataset
  RooDataSet Data("Data", "Data", VarSet, Import(Bs2mumu_Data));
  Data.append(CBG_Data);

    // Plot the full data sample
    
  if(t == 0) {

    TCanvas cMassGen1("All generated mass - " + BDTbin, "All generated mass - " + BDTbin, 1);
    RooPlot* pMassGen1 = B_s0_M->frame(Bins(25));
    Format_Plot(pMassGen1); 
    Data.plotOn(pMassGen1) ;
    pMassGen1->Draw();
    cMassGen1.Write();
      
    TCanvas cTimeGen1("All generated decay time - " + BDTbin, "All generated decay time - " + BDTbin, 1);
    RooPlot* pTimeGen1 = B_s0_TAU->frame(Bins(100));
    Format_Plot(pTimeGen1);
    Data.plotOn(pTimeGen1) ;
    pTimeGen1->Draw();
    cTimeGen1.Write();

  }

  //--------------------------------
  // Build Fit Model
  //--------------------------------

  //--------------------------------
  // Bs2mumu
  //--------------------------------
    
    // Setup the mass PDF
    
  Double_t mean_val = Bs2mumu_mean_val;
  Double_t sigma_val= Bs2mumu_sigma_val;
  Double_t alpha_val_1= Bs2mumu_alpha_val_1;
  Double_t n_val_1= Bs2mumu_n_val_1;
  Double_t alpha_val_2= Bs2mumu_alpha_val_2;
  Double_t n_val_2= Bs2mumu_n_val_2;

  Double_t mean_val_prev = Bs2mumu_mean_val_prev;
  Double_t sigma_val_prev = Bs2mumu_sigma_val_prev;
  Double_t alpha_val_prev = Bs2mumu_alpha_val_prev;
  Double_t n_val_prev = Bs2mumu_n_val_prev;

  RooRealVar Bs2mumu_mean("Bs2mumu_mean","Bs2mumu_mean", mean_val);
  RooRealVar Bs2mumu_sigma("Bs2mumu_sigma", "Bs2mumu_sigma", sigma_val);
  RooRealVar Bs2mumu_alpha_1("Bs2mumu_alpha_1", "Bs2mumu_alpha_1", alpha_val_1);
  RooRealVar Bs2mumu_n_1("Bs2mumu_n_1"    , "Bs2mumu_n_1"    , n_val_1);
  RooRealVar Bs2mumu_alpha_2("Bs2mumu_alpha_2", "Bs2mumu_alpha_2", alpha_val_2);
  RooRealVar Bs2mumu_n_2("Bs2mumu_n_2"    , "Bs2mumu_n_2"    , n_val_2);

  RooRealVar Bs2mumu_mean_prev("Bs2mumu_mean_prev","Bs2mumu_mean_prev", mean_val_prev);
  RooRealVar Bs2mumu_sigma_prev("Bs2mumu_sigma_prev", "Bs2mumu_sigma_prev", sigma_val_prev);
  RooRealVar Bs2mumu_alpha_prev("Bs2mumu_alpha_prev", "Bs2mumu_alpha_prev", alpha_val_prev);
  RooRealVar Bs2mumu_n_prev("Bs2mumu_n_prev"    , "Bs2mumu_n_prev"    , n_val_prev);
    
  RooDoubleCBShape Bs2mumu_Mass_PDF("Bs2mumu_Mass_PDF", "Bs2mumu_Mass_PDF", *B_s0_M, Bs2mumu_mean, Bs2mumu_sigma, Bs2mumu_alpha_1, Bs2mumu_n_1,
    Bs2mumu_alpha_2, Bs2mumu_n_2);

  // Define CB shape used in previous analysis
  RooCBShape Bs2mumu_Mass_PDF_prev("Bs2mumu_Mass_PDF_prev", "Bs2mumu_Mass_PDF_prev", *B_s0_M, Bs2mumu_mean_prev, Bs2mumu_sigma_prev, Bs2mumu_alpha_prev,
   Bs2mumu_n_prev);

  //--------------------------------
  // Combinatorial background
  //--------------------------------
    
  // Setup the mass PDF 
    
  RooRealVar CBG_k("CBG_k" , "CBG_k" , k_val, -10.0, 10.0);

  RooExponential CBG_Mass_PDF("CBG_Mass_PDF", "CBG_Mass_PDF", *B_s0_M, CBG_k);

  CBG_k.setConstant(Fix_CBG_Slope);


  //------------------------------------------------
  // Create the yield variables
  // ------------------------------------------------

  // Get the total number of events in the dataset
  Double_t nGen = Data.sumEntries();

  RooRealVar Bs2mumu_Yield    ("Bs2mumu_Yield"     , "Bs2mumu_Yield"     , Yield_in_Fit_Range[0], 0.0, (Double_t)nGen*2);
  RooRealVar CBG_Yield        ("CBG_Yield"         , "CBG_Yield"         , Yield_in_Fit_Range[1], 0.0, (Double_t)nGen*2);

  //------------------------------------------------
  // Build the total fitting PDF
  //------------------------------------------------

  RooArgList Fit_PDFs, Yields;
  if(UseRunIIPDF == 0) 
    Fit_PDFs.add(Bs2mumu_Mass_PDF);
  else if(UseRunIIPDF == 1)
    Fit_PDFs.add(Bs2mumu_Mass_PDF_prev);
  Fit_PDFs.add(CBG_Mass_PDF);
    
  Yields.add(Bs2mumu_Yield);
  Yields.add(CBG_Yield);
        
  RooAddPdf Fit_PDF("Fit_PDF", "Fit_PDF", Fit_PDFs, Yields);

  //------------------------------------------------
  // Fit the data
  //------------------------------------------------

  if(t == 0)
    Fit_PDF.fitTo(Data, Extended(kTRUE), Minos(kTRUE));
  else
    Fit_PDF.fitTo(Data, Extended(kTRUE), Minos(kTRUE), Verbose(kFALSE), PrintLevel(-1), PrintEvalErrors(-1));
    
  //------------------------------------------------
  // Plot the results
  //------------------------------------------------

  if(t == 0) {    
     
    // Calculate the appropriate number of bins within the new range
    Int_t nMBins = (Int_t)(nBins*(FitMax-FitMin)/(MassMax-MassMin));

    TCanvas cMassFit("Fitted mass - " + BDTbin, "Fitted mass - " + BDTbin, 1);
    RooPlot* pMassFit = B_s0_M->frame(Bins(nMBins));
    Format_Plot(pMassFit);
    Data.plotOn(pMassFit) ;
    Fit_PDF.plotOn(pMassFit);
    if(UseRunIIPDF == 0) 
      Fit_PDF.plotOn(pMassFit, Components(Bs2mumu_Mass_PDF), LineColor(kRed));
    else if(UseRunIIPDF == 1)
      Fit_PDF.plotOn(pMassFit, Components(Bs2mumu_Mass_PDF_prev), LineColor(kRed));

    Fit_PDF.plotOn(pMassFit, Components(CBG_Mass_PDF), LineColor(kBlue), LineStyle(kDashed));
        
    pMassFit->Draw();
    cMassFit.Write();

    cout << "Fitted yields vs expected yields before sWeighting - " <<  BDTbin << endl;
    cout << "Bs  :: " << Bs2mumu_Yield.getVal() << " +/- " << Bs2mumu_Yield.getError() << "  vs expected yield = " << Yield_in_Fit_Range[0] << " and actual generated yield " << ActualYield[0] << endl;
    cout << "CBG :: " << CBG_Yield.getVal() << " +/- " << CBG_Yield.getError() << "  vs  expected yield = " << Yield_in_Fit_Range[1] << " and actual generated yield " << ActualYield[1] << endl;

    }
       
    // Create copies of the yield observables so they don't get messed up by the internal fit in
    RooRealVar Bs2mumu_Yield_res = Bs2mumu_Yield;
    RooRealVar CBG_Yield_res =  CBG_Yield;
    RooRealVar CBG_k_res = CBG_k;
       

    //------------------------------------------------
    // Add sWeights to the data
    //------------------------------------------------
     
    // Set all non-yield variables constant
    CBG_k.setConstant(kTRUE);
  
    // Now use SPlot to add SWeights to the data set based on the model and the yield variables
    RooStats::SPlot sPlotObject("sPlotObject","sPlotObject", Data, &Fit_PDF, Yields); 
     
    // Create the sWeighted data
    RooDataSet sData(Data.GetName(), Data.GetTitle(), &Data, *Data.get(), 0, "Bs2mumu_Yield_sw");
    
    //---------------------------------------------------------------------------
    // Replace the sWeights by renormalised sWeights: sW = sW * Sum(sW)/Sum(sW^2)
    //---------------------------------------------------------------------------

    RooRealVar sW2("sW2", "sW2", -1000.0, 1000.0);

    // Create a new dataset
    
    RooDataSet sData2("sData2", "sData2", RooArgSet(*B_s0_TAU, sW2));

    Double_t sum_sWeight = 0;
    Double_t sum_sWeight2 = 0;
  
    const RooArgSet* obs = sData.get();
    RooRealVar* B_s0_TAU2 = (RooRealVar*)obs->find(B_s0_TAU->GetName());

    for(int w = 0; w < sData.numEntries(); w++) {
      sData.get(w);
      sum_sWeight += sData.weight();
      sum_sWeight2 += sData.weight()*sData.weight();
    }

    Double_t alpha_weight = 1.0;//sum_sWeight/sum_sWeight2; //this will rescale the weights
     
    // Loop over data again and input into new dataset with new weights
    for(int w = 0; w < sData.numEntries(); w++) {
      sData.get(w);
      Double_t sWeight = sData.weight();
      Double_t sWeight2 = sWeight*alpha_weight;
      sW2 = sWeight2;
      *B_s0_TAU = B_s0_TAU2->getVal();
      sData2.add(RooArgSet(*B_s0_TAU, sW2));
    }
      
    RooDataSet wData(sData2.GetName(),sData2.GetTitle(), &sData2, *sData2.get(), 0, sW2.GetName()) ;

    if(t == 0) {

      cout << "Fitted yields vs expected yields after sWeighting - " << BDTbin <<  endl;
      cout << "Bs  :: " << Bs2mumu_Yield.getVal() << " +/- " << Bs2mumu_Yield.getError() << "  vs expected yield = " << Yield_in_Fit_Range[0] << " and actual generated yield " << ActualYield[0] << endl;
      cout << "CBG :: " << CBG_Yield.getVal() << " +/- " << CBG_Yield.getError() << "  vs  expected yield = " << Yield_in_Fit_Range[1] << " and actual generated yield " << ActualYield[1] << endl;

      // Check that the weights have the desired properties
      cout << endl;
      cout << "Yields from sWeights - " << BDTbin << endl;
      cout << "--------------------" << endl;
      cout << "Bs2mumu = " << sPlotObject.GetYieldFromSWeight("Bs2mumu_Yield") << endl;
      cout << "CBG     = " << sPlotObject.GetYieldFromSWeight("CBG_Yield") << endl;
      cout << "--------------------" << endl;
      cout << endl;
        
      // Plot the sWeighted decay time distribution
      TCanvas cTau("sWeighted decay time -" + BDTbin , "sWeighted decay time -" + BDTbin, 1);
      RooPlot* pTau = B_s0_TAU->frame(Bins(nBins)) ;
      Format_Plot(pTau);
      Data.plotOn(pTau, DataError(RooAbsData::SumW2)) ;
      sData.plotOn(pTau, DataError(RooAbsData::SumW2), MarkerColor(kRed));
      wData.plotOn(pTau, DataError(RooAbsData::SumW2), MarkerColor(kGreen));
      pTau->Draw();
      cTau.Write();
    }

    return std::make_tuple(Bs2mumu_Yield_res, CBG_Yield_res, CBG_k_res, wData);

}

//-------------------------------------------------------------
//
// This function plots the likelihood of a fit variable
//
//------------------------------------------------------------

void Plot_Likelihood_Scan(RooRealVar& Var, RooAbsReal* nll){

  RooAbsReal* Var_LL = nll->createProfile(Var);
  TCanvas cVar_LL(Var.getTitle()+ " LL", Var.getTitle()+ " LL", 1);
  RooPlot* pVar = Var.frame();
  nll->plotOn(pVar, LineColor(kBlue), ShiftToZero());
  Var_LL->plotOn(pVar, LineColor(kRed));
  pVar->Draw();
  cVar_LL.Write();
  return;
}

void Fill_Tree( TTree &Results_Tree, RooRealVar &Var, double Input, double (&Results)[12], Int_t t ){

  if( t == 0) {
    TString VarName = Var.getTitle();
    TString ResultNames[12] = { "_Val", "_True", "_Err", "_ErrLo", "_ErrHi", "_FracDev", "_Pull", "_Pull_Minos", "_Val_MinosSymmetric", "_Err_MinosSymmetric", "_FracDev_MinosSymmetric", "_Pull_MinosSymmetric" };
    for(int i = 0; i < 12; i++) {
      Results_Tree.Branch(VarName+ResultNames[i], &Results[i]);
    }
  }

  Results[0] = Var.getVal();
  Results[1] = Input;
  Results[2] = Var.getError();
  Results[3] = -Var.getErrorLo();
  Results[4] = Var.getErrorHi();
  Results[5] = (Results[0]-Results[1])/Results[1];
  Results[6] = (Results[0]-Results[1])/Results[2];
  if( Results[0] > Results[1] ) 
    Results[7] = (Results[0]-Results[1])/Results[3];
  else
    Results[7] = (Results[0]-Results[1])/Results[4];
  Results[8] = (Results[0]+Results[4])-(Results[3]+Results[4])/2.0; // Val_MinosSymmetric
  Results[9] = (Results[3]+Results[4])/2.0; // Err_MinosSymmetric
  Results[10] = (Results[8] - Results[1])/Results[1]; // FracDev_MinosSymmetric
  Results[11] = (Results[8] - Results[1])/Results[9]; // Pull_MinosSymmetric

  return;
}

void Fill_Yield_Tree( TTree &Results_Tree, RooRealVar &Var, double Input, int Actual, double (&Results)[16], Int_t t ){

  if( t == 0) {
    TString VarName = Var.getTitle();
    TString ResultNames[16] = { "_Val", "_True", "_Actual", "_Err", "_ErrLo", "_ErrHi", "_FracDevTrue", "_PullTrue", "_Pull_MinosTrue", "_FracDevActual", "_PullActual", "_Pull_MinosActual", "_Val_MinosSymmetric", "_Err_MinosSymmetric", "_FracDev_MinosSymmetric", "_Pull_MinosSymmetric" };

    for(int i = 0; i < 16; i++) {

      Results_Tree.Branch(VarName+ResultNames[i], &Results[i]);

    }
  }

  Results[0] = Var.getVal();
  Results[1] = Input;
  Results[2] = (double)Actual;
  Results[3] = Var.getError();
  Results[4] = -Var.getErrorLo();
  Results[5] = Var.getErrorHi();
  Results[6] = (Results[0]-Results[1])/Results[1]; // FracDev
  Results[7] = (Results[0]-Results[1])/Results[3]; // Pull
  if( Results[0] > Results[1] )                    // PullMinos
    Results[8] = (Results[0]-Results[1])/Results[4];
  else
    Results[8] = (Results[0]-Results[1])/Results[5];
  
  Results[9] = (Results[0]-Results[2])/Results[2]; // FracDev
  Results[10] = (Results[0]-Results[2])/Results[3]; // Pull
  if( Results[0] > Results[2] )                    // PullMinos
    Results[11] = (Results[0]-Results[2])/Results[4];
  else
    Results[11] = (Results[0]-Results[2])/Results[5];
  Results[12] = (Results[0]+Results[5])-(Results[4]+Results[5])/2.0; // Val_MinosSymmetric
  Results[13] = (Results[4]+Results[5])/2.0; // Err_MinosSymmetric
  Results[14] = (Results[12] - Results[1])/Results[1]; // FracDev_MinosSymmetric
  Results[15] = (Results[12] - Results[1])/Results[13];

  return;
}

