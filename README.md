# AsymptoticErrorSimFit

Basic (removed all exclusive backgrounds and Gaussian constraints on acceptance parameters) version of the simultaneous toy fit. For testing whether RooSimultaneous is compatible with AsymptoticError fit option.

To run:

``
root .L RooNewBinAccExponential.cpp RooBinAccExponential.cpp RooDoubleCBShape.cpp Lifetime_Fit_v13.cpp
``

I am not rescaling the weights as the AsymptoticError flag means we do not need to rescale the sWeights (in theory), therefore in Lifetime_Fit_v13.h

``
Double_t alpha_weight = 1.0;//sum_sWeight/sum_sWeight2; //this will rescale the weights
``