/*****************************************************************************
 * Project: RooFit                                                           *
 * Package: RooFitModels                                                     *
 * @(#)root/roofit:$Id: RooAccExponential.cxx 24286 2008-06-16 15:47:04Z wouter $
 * Authors:                                                                  *
 *   WV, Wouter Verkerke, UC Santa Barbara, verkerke@slac.stanford.edu       *
 *   DK, David Kirkby,    UC Irvine,         dkirkby@uci.edu                 *
 *                                                                           *
 * Copyright (c) 2000-2005, Regents of the University of California          *
 *                          and Stanford University. All rights reserved.    *
 *                                                                           *
 * Redistribution and use in source and binary forms,                        *
 * with or without modification, are permitted according to the terms        *
 * listed in LICENSE (http://roofit.sourceforge.net/license.txt)             *
 *****************************************************************************/

//////////////////////////////////////////////////////////////////////////////
//
// BEGIN_HTML
// RooBinAccExponential is a RooAbsPdf implementing an acceptance function multiplied by an exponential decay time distribution for use with an
// upper and lower BDT cut
// END_HTML
//

#include "RooFit.h"

#include "Riostream.h"
#include "Riostream.h"
#include <math.h>

#include "RooBinAccExponential.h"
#include "RooRealVar.h"
#include "RooRealConstant.h"
#include "RooMath.h"
#include "TMath.h"

class RooBinAccExponential;


//_____________________________________________________________________________
RooBinAccExponential::RooBinAccExponential(const char *name, const char *title,
				     RooAbsReal& _t, RooAbsReal& _t0, RooAbsReal& _a, RooAbsReal& _b, RooAbsReal& _gamma) :
  RooAbsPdf(name, title), 
  t("t","Time",this,_t),
  t0("t0","Minimum Time",this,_t0),
  a("a","a",this,_a),
  b("b","b",this,_b),
  gamma("gamma","-1/Lifetime",this,_gamma)
{
}



//_____________________________________________________________________________
RooBinAccExponential::RooBinAccExponential(const RooBinAccExponential& other, const char* name) :
  RooAbsPdf(other,name), 
  t("t",this,other.t),
  t0("t0",this,other.t0),
  a("a",this,other.a),
  b("b",this,other.b),
  gamma("gamma",this,other.gamma)

{
}



//_____________________________________________________________________________
Double_t RooBinAccExponential::evaluate() const {
 
  Double_t Acc = exp(-0.5 * pow(((log(t-t0)-a)/b),2));

  if(t <= t0){
    Acc = 0.0;
  }

  Double_t Exponential = exp(t*gamma);

  Double_t val = Acc*Exponential;


  return val;



}


