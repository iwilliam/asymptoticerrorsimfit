/*****************************************************************************
 * Project: RooFit                                                           *
 * Package: RooFitModels                                                     *
 *    File: $Id: RooBinAccExponential.h,v 1.13 2007/07/12 20:30:49 wouter Exp $
 * Authors:                                                                  *
 *   WV, Wouter Verkerke, UC Santa Barbara, verkerke@slac.stanford.edu       *
 *   DK, David Kirkby,    UC Irvine,         dkirkby@uci.edu                 *
 *                                                                           *
 * Copyright (c) 2000-2005, Regents of the University of California          *
 *                          and Stanford University. All rights reserved.    *
 *                                                                           *
 * Redistribution and use in source and binary forms,                        *
 * with or without modification, are permitted according to the terms        *
 * listed in LICENSE (http://roofit.sourceforge.net/license.txt)             *
 *****************************************************************************/
#ifndef ROO_BIN_ACC_EXPONENTIAL
#define ROO_BIN_ACC_EXPONENTIAL

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;
class RooAbsReal;

class RooBinAccExponential : public RooAbsPdf {
public:
  RooBinAccExponential() {} ;
  RooBinAccExponential(const char *name, const char *title, 
		    RooAbsReal& _t, RooAbsReal& _t0, RooAbsReal& _a, RooAbsReal& _b, RooAbsReal& _gamma);
  RooBinAccExponential(const RooBinAccExponential& other,const char* name=0) ;
  virtual TObject* clone(const char* newname) const { return new RooBinAccExponential(*this,newname); }
  inline virtual ~RooBinAccExponential() { }

//  Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const ;
//  Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;

protected:
  RooRealProxy t ;
  RooRealProxy t0 ;
  RooRealProxy a ;
  RooRealProxy b ;
  RooRealProxy gamma ;

  Double_t evaluate() const ;
//   void initGenerator();

private:
  //ClassDef(RooBinAccExponential,2) // Acceptance function multiplied by an exponential decay time distribution
};

#endif

