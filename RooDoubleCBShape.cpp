#include "RooDoubleCBShape.h"

#include <iostream>
#include <math.h>
#include "TMath.h"
#include "RooMath.h"

#include "RooRealVar.h"
#include "RooRealConstant.h"

using namespace RooFit;

 RooDoubleCBShape::RooDoubleCBShape(const char *name, const char *title,
        RooAbsReal& _x,
        RooAbsReal& _mean,
        RooAbsReal& _width,
        RooAbsReal& _alpha1,
        RooAbsReal& _n1,
        RooAbsReal& _alpha2,
        RooAbsReal& _n2
        ) :
   RooAbsPdf(name,title),
   x("x","x",this,_x),
   mean("mean","mean",this,_mean),
   width("width","width",this,_width),
   alpha1("alpha1","alpha1",this,_alpha1),
   n1("n1","n1",this,_n1),
   alpha2("alpha2","alpha2",this,_alpha2),
   n2("n2","n2",this,_n2)
 {
 }


 RooDoubleCBShape::RooDoubleCBShape(const RooDoubleCBShape& other, const char* name) :
   RooAbsPdf(other,name),
   x("x",this,other.x),
   mean("mean",this,other.mean),
   width("width",this,other.width),
   alpha1("alpha1",this,other.alpha1),
   n1("n1",this,other.n1),
   alpha2("alpha2",this,other.alpha2),
   n2("n2",this,other.n2)

 {
 }

 double RooDoubleCBShape::evaluate() const
 {
   double A1 = pow(n1/fabs(alpha1),n1)*exp(-alpha1*alpha1/2);
   double A2 = pow(n2/fabs(alpha2),n2)*exp(-alpha2*alpha2/2);
   double B1 = n1/fabs(alpha1)-fabs(alpha1);
   double B2 = n2/fabs(alpha2)-fabs(alpha2);

   if((x-mean)/width>-alpha1 && (x-mean)/width<alpha2){
     return exp(-(x-mean)*(x-mean)/(2*width*width));
   }
   else if((x-mean)/width<-alpha1){
     return A1*pow(B1-(x-mean)/width,-n1);
   }
   else if((x-mean)/width>alpha2){
     return A2*pow(B2+(x-mean)/width,-n2);
   }
   else{
     std::cout << "ERROR evaluating range..." << std::endl;

     return 99;
   }
 }


Int_t RooDoubleCBShape::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char*) const
{
  if (matchArgs(allVars,analVars,x)) return 1 ;
  return 0 ;
}

Double_t RooDoubleCBShape::analyticalIntegral(Int_t code, const char* r) const {

  double umin = (x.min(r) - mean) / width;
  double umax = (x.max(r) - mean) / width;
  R__ASSERT(code==1);

  double integral = 0.;

  if (umin>=umax) return 0;

  if (umin < -TMath::Abs(alpha1)) {
    if (umax < -TMath::Abs(alpha1)) {
      integral = IntPwLwSx(umin,umax,alpha1,n1);
    } else if ((umax >= -TMath::Abs(alpha1)) && (umax < TMath::Abs(alpha2))) {
      integral += IntPwLwSx(umin,-alpha1,alpha1,n1);
      integral += IntGaus(-alpha1,umax);
    } else if (umax >= TMath::Abs(alpha2)) {
      integral += IntPwLwSx(umin,-alpha1,alpha1,n1);
      integral += IntGaus(-alpha1,alpha2);
      integral += IntPwLwDx(alpha2,umax,alpha2,n2);
    }
  } else if ((umin>= -TMath::Abs(alpha1)) && (umin<TMath::Abs(alpha2))) {
    if (umax<TMath::Abs(alpha2)) {
      integral = IntGaus(umin,umax);
    } else if (umax >= TMath::Abs(alpha2)) {
      integral += IntGaus(umin,alpha2);
      integral += IntPwLwDx(alpha2,umax,alpha2,n2);
    }
  } else if (umin>=TMath::Abs(alpha2)) {
    integral = IntPwLwDx(umin,umax,alpha2,n2);
  }



  //integral += IntPwLw(TMath::Max(-umax, TMath::Abs(alpha1)), TMath::Max(-umin, TMath::Abs(alpha1)), alpha1, n1);
  //integral += IntGaus(TMath::Max(umin, -TMath::Abs(alpha1)), TMath::Min(umax, TMath::Abs(alpha2)));
  //integral += IntPwLw(TMath::Max(umin, TMath::Abs(alpha2)), TMath::Max(umax, TMath::Abs(alpha2)), alpha2, n1);


  return width * integral;
}


Double_t RooDoubleCBShape::ApproxErf(Double_t arg) const
{
  static const double erflim = 5.0;
  if( arg > erflim )
    return 1.0;
  if( arg < -erflim )
    return -1.0;

  return RooMath::erf(arg);
}

double RooDoubleCBShape::IntGaus(double x0, double x1) const
{
  static const double rootPiBy2 = TMath::Sqrt(TMath::PiOver2());

  if (x0 >= x1) return 0; // needed in case umin > alpha2

  // N.B. Erf is integral from 0
     if (x0*x1<0) // they are at different side of zero
       {
       return rootPiBy2 * ( ApproxErf(TMath::Abs(x1) / TMath::Sqrt2()) + ApproxErf(TMath::Abs(x0) / TMath::Sqrt2()) );
       }
       else //They are at the same side of zero
       {

       return rootPiBy2 * TMath::Abs( ApproxErf(TMath::Abs(x1) / TMath::Sqrt2()) - ApproxErf(TMath::Abs(x0) / TMath::Sqrt2()) );
       }


     //return rootPiBy2 * (ApproxErf(x1 / TMath::Sqrt2()) - ApproxErf(x0 / TMath::Sqrt2()) );


}

double RooDoubleCBShape::IntPwLwDx(double x0, double x1, double alpha, double n) const
{

  if (x0 == x1) return 0; // already implicit below but so it's clear

  bool useLog = false;
  if(fabs(n - 1.0) < 1.0e-05)
    useLog = true;

  double A  = TMath::Power(n/TMath::Abs(alpha),n)*TMath::Exp(-alpha*alpha/2);
  double B  = n/TMath::Abs(alpha) - TMath::Abs(alpha);

  double result = 0.;
  if(useLog)
    {
      result = A * ( TMath::Log(B + x1) - TMath::Log(B + x0));
    }
  else
    {
      result = A / (1. - n) * ( TMath::Power(B + x1, 1. - n) - TMath::Power(B + x0, 1. - n) );
    }
  return result;
}

double RooDoubleCBShape::IntPwLwSx(double x0, double x1, double alpha, double n) const
{

  if (x0 == x1) return 0; // already implicit below but so it's clear

  bool useLog = false;
  if(fabs(n - 1.0) < 1.0e-05)
    useLog = true;

  double A  = TMath::Power(n/TMath::Abs(alpha),n)*TMath::Exp(-alpha*alpha/2);
  double B  = n/TMath::Abs(alpha) - TMath::Abs(alpha);

  double result = 0.;
  if(useLog)
    {
      result = - A * ( TMath::Log(B - x1) - TMath::Log(B - x0));
    }
  else
    {
      result = - A / (1. - n) * ( TMath::Power(B - x1, 1. - n) - TMath::Power(B - x0, 1. - n) );
    }
  return result;
}

Int_t RooDoubleCBShape::getMaxVal(const RooArgSet& vars) const
  {
    RooArgSet dummy ;

    if (matchArgs(vars,dummy,x)) {
      return 1 ;
    }
    return 0 ;
  }

Double_t RooDoubleCBShape::maxVal(Int_t code) const
  {
    R__ASSERT(code==1) ;

    // The maximum value for given (m0,alpha,n,sigma)
    return 1.0 ;
  }