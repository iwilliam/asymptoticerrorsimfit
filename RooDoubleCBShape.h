/*****************************************************************************
 * Project: RooFit                                                           *
 *                                                                           *
 * Copyright (c) 2000-2005, Regents of the University of California          *
 *                          and Stanford University. All rights reserved.    *
 *                                                                           *
 * Redistribution and use in source and binary forms,                        *
 * with or without modification, are permitted according to the terms        *
 * listed in LICENSE (http://roofit.sourceforge.net/license.txt)             *
 *****************************************************************************/

#ifndef ROODOUBLECBSHAPE_H
#define ROODOUBLECBSHAPE_H

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;
class RooAbsReal;

class RooDoubleCBShape : public RooAbsPdf {
public:
  RooDoubleCBShape() {};
  RooDoubleCBShape(const char *name, const char *title,
          RooAbsReal& _x,
          RooAbsReal& _mean,
          RooAbsReal& _width,
          RooAbsReal& _alpha1,
          RooAbsReal& _n1,
          RooAbsReal& _alpha2,
          RooAbsReal& _n2
       );
  RooDoubleCBShape(const RooDoubleCBShape& other, const char* name=0) ;
  virtual TObject* clone(const char* newname) const { return new RooDoubleCBShape(*this,newname); }
  inline virtual ~RooDoubleCBShape() { }

  virtual Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* r=0) const;
  virtual Double_t analyticalIntegral(Int_t code,const char* rangeName=0) const;

  virtual Int_t getMaxVal(const RooArgSet& vars) const ;
  virtual Double_t maxVal(Int_t code) const ;

protected:

  RooRealProxy x ;
  RooRealProxy mean;
  RooRealProxy width;
  RooRealProxy alpha1;
  RooRealProxy n1;
  RooRealProxy alpha2;
  RooRealProxy n2;

  Double_t evaluate() const ;
  double IntGaus(double x0, double x1) const;
  double IntPwLwDx(double x0, double x1, double alpha, double n) const;
  double IntPwLwSx(double x0, double x1, double alpha, double n) const;
  Double_t ApproxErf(Double_t arg) const;

private:

  //ClassDef(RooDoubleCBShape,1) // Your description goes here...
};

#endif

