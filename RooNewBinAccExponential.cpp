/*****************************************************************************
 * Project: RooFit                                                           *
 * Package: RooFitModels                                                     *
 * @(#)root/roofit:$Id: RooNewBinAccExponential.cxx 24286 2008-06-16 15:47:04Z wouter $
 * Authors:                                                                  *
 *   WV, Wouter Verkerke, UC Santa Barbara, verkerke@slac.stanford.edu       *
 *   DK, David Kirkby,    UC Irvine,         dkirkby@uci.edu                 *
 *                                                                           *
 * Copyright (c) 2000-2005, Regents of the University of California          *
 *                          and Stanford University. All rights reserved.    *
 *                                                                           *
 * Redistribution and use in source and binary forms,                        *
 * with or without modification, are permitted according to the terms        *
 * listed in LICENSE (http://roofit.sourceforge.net/license.txt)             *
 *****************************************************************************/

//////////////////////////////////////////////////////////////////////////////
//
// BEGIN_HTML
// RooNewBinAccExponential is a RooAbsPdf implementing an acceptance function which is the sum of two RooBinAccExponential functions multiplied by an exponential decay time distribution for use with an
// upper and lower BDT cut
// END_HTML
//

#include "RooFit.h"

#include "Riostream.h"
#include "Riostream.h"
#include <math.h>

#include "RooNewBinAccExponential.h"
#include "RooRealVar.h"
#include "RooRealConstant.h"
#include "RooMath.h"
#include "TMath.h"

class RooNewBinAccExponential;


//_____________________________________________________________________________
RooNewBinAccExponential::RooNewBinAccExponential(const char *name, const char *title,
				     RooAbsReal& _t, RooAbsReal& _a, RooAbsReal& _b, RooAbsReal& _c, RooAbsReal& _d, RooAbsReal& _e,
            RooAbsReal& _gamma) :
  RooAbsPdf(name, title), 
  t("t","Time",this,_t),
  a("a","a",this,_a),
  b("b","b",this,_b),
  c("c","c",this,_c),
  d("d","d",this,_d),
  e("e","e",this,_e),
  gamma("gamma","-1/Lifetime",this,_gamma)
{
}



//_____________________________________________________________________________
RooNewBinAccExponential::RooNewBinAccExponential(const RooNewBinAccExponential& other, const char* name) :
  RooAbsPdf(other,name), 
  t("t",this,other.t),
  a("a",this,other.a),
  b("b",this,other.b),
  c("c",this,other.c),
  d("d",this,other.d),
  e("e",this,other.e),
  gamma("gamma",this,other.gamma)

{
}



//_____________________________________________________________________________
Double_t RooNewBinAccExponential::evaluate() const {

  Double_t Acc = a * erf(t * pow(b * tanh(c * pow(t,3)), 0.5)) + (d * pow(t,2)) - (e * t);

  if(t < 0.26){
    Acc = 0.0;
  }
  
  Double_t Exponential = exp(t*gamma);

  Double_t val = Acc*Exponential;


  return val;


}



//_____________________________________________________________________________
//Int_t RooNewBinAccExponential::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/) const
//{
//
//  if (matchArgs(allVars,analVars,m)) return 1;

//  return 0;

//}



//_____________________________________________________________________________
//Double_t RooNewBinAccExponential::analyticalIntegral(Int_t code, const char* rangeName) const
//{
//  assert(code==1);
//
//  return 1;
//}


